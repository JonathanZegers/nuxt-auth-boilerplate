# nuxt-auth-boilerplate

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

## Folder Structure

#### API
#### Assets
#### Components
#### layouts
#### Middleware
#### pages
#### plugins
#### server
#### static
#### store

#### root folder
##### .env
##### .gitlab-ci.yml
##### nuxt.config.js
##### package.json
##### Procfile

## Packages And Use
* "@nuxtjs/auth": "^4.8.5"   
* "@nuxtjs/axios": "^5.9.3",
* "bcryptjs": "^2.4.3",
* "bootstrap": "^4.1.3",
* "bootstrap-vue": "^2.0.0",
* "cross-env": "^5.2.0",
* "dotenv": "^8.2.0",
* "express": "^4.16.4",
* "express-validator": "^6.3.1",
* "jsonwebtoken": "^8.5.1",
* "mongoose": "^5.8.7",
* "nodemailer": "^6.5.0",
* "nuxt": "^2.0.0"

