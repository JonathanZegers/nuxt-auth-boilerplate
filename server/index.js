// Express middleware for backend development
const express = require('express')

// A elegant console logger (npmjs.com/package/consola)
const consola = require('consola')

const { Nuxt, Builder } = require('nuxt')

const app = express()

const config = require('../nuxt.config.js')

// sets the dev config to true or false based on NODE_ENV variable
config.dev = process.env.NODE_ENV !== 'production'

async function start() {
  // Init Nuxt.js using config file
  const nuxt = new Nuxt(config)

  const { host, port } = nuxt.options.server

  // If we are in dev mode, build the server
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  } else {
    await nuxt.ready()
  }

  // Gives nuxt middleware over to express
  app.use(nuxt.render)

  // Listen to the server
  app.listen(port, host)
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true,
  })
}

start()
