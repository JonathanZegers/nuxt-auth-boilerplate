require('dotenv').config()
const mongoose = require('mongoose')

const USERNAME = process.env.DB_USER
const PASSWORD = process.env.DB_PASS
const COLLECTION = process.env.DB_COLLECTION
const AXIOSURL = process.env.BASE_URL

const DB_URI = 'mongodb://localhost:27017/myapp'

mongoose.connect(DB_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true,
})

var db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function callback() {
  console.log('MongoDB Connected to: ' + DB_URI)
})

module.exports = db
