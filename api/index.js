const express = require('express')

// initialized the DB connection
const db = require('./db')


const app = express()

// Init body-parser option (now included with express)
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// Import API routes
const users = require('./routes/userRoutes')
//const posts = require('./routes/posts')

// Use API routes
app.use(users)
//app.use(posts)

// Export the server middleware for use in nuxt.config
module.exports = {
  path: '/api',
  handler: app,
}
